// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyBkn3pBmpqHfLzirpEAJYTUqJnMEXPXLTM",
    authDomain: "fir-authentication-9b437.firebaseapp.com",
    databaseURL: "https://fir-authentication-9b437.firebaseio.com",
    projectId: "fir-authentication-9b437",
    storageBucket: "fir-authentication-9b437.appspot.com",
    messagingSenderId: "532977614638",
    appId: "1:532977614638:web:ae9c0c64b2ae64308d44bf"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
