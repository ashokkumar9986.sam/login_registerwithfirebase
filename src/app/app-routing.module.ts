import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './LoginPage/login/login.component';
import { RegisterComponent } from './LoginPage/register/register.component';
import { ResetPasswordComponent } from './LoginPage/reset-password/reset-password.component';
import { UserInfoComponent } from './LoginPage/user-info/user-info.component';
import { VerifyEmailAddressComponent } from './LoginPage/verify-email-address/verify-email-address.component';

const routes: Routes = [
  {
    path:'', redirectTo:'/login', pathMatch:'full'
  },
  {
    path:'login', component:LoginComponent
  },
  {
    path:'register', component:RegisterComponent
  },
  {
    path:'userInfo', component:UserInfoComponent
  },
  {
    path: 'forget-Password', component: ResetPasswordComponent
  },
  {
    path: 'verify-email-address', component: VerifyEmailAddressComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
