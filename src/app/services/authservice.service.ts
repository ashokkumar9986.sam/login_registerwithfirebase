// import { Injectable } from '@angular/core';
// import { AngularFireAuth } from '@angular/fire/auth';
// import { Router } from '@angular/router';
// import { auth } from 'firebase';

import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {Router} from '@angular/router';
import { auth } from 'firebase/app';
import * as firebase from 'firebase/app';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { User } from './user';


@Injectable({
  providedIn: 'root'
})
export class AuthserviceService {

  authState: any;

  constructor(private afu: AngularFireAuth, private router: Router, private afs: AngularFirestore,
    private ngZone: NgZone) { 
    this.afu.authState.subscribe((auth =>{
      // this.authState = auth;
      if (auth) {
        this.authState = auth;
        localStorage.setItem('user', JSON.stringify(this.authState));
        JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    }))
  }

  // all firebase getdata functions

  get isUserAnonymousLoggedIn(): boolean {
    return (this.authState !== null) ? this.authState.isAnonymous : false
  }

  get currentUserId(): string {
    return (this.authState !== null) ? this.authState.uid : ''
  }

  get currentUserName(): string {
    return this.authState['email']
  }

  get currentUser(): any {
    return (this.authState !== null) ? this.authState : null;
  }

  get isUserEmailLoggedIn(): boolean {
    if ((this.authState !== null) && (!this.isUserAnonymousLoggedIn)) {
      return true
    } else {
      return false
    }
  }

  registerWithEmail(email: string, password: string) {
    return this.afu.createUserWithEmailAndPassword(email, password)
      // .then((user) => {
      //   this.SendVerificationMail();
      //   this.authState = user
      // })
      .then((result) => {
        if (result.user.emailVerified !== true) {
          this.SendVerificationMail();
          window.alert('Please validate your email address. Kindly check your inbox.');
        } else {
          this.ngZone.run(() => {
            this.router.navigate(['login']);
          });
        }
        this.SetUserData(result.user);
      })
      .catch(error => {
        // console.log(error)
        throw error
      });
  }

  loginWithEmail(email: string, password: string)
  {
    return this.afu.signInWithEmailAndPassword(email, password)
      .then((user) => {
        // this.SendVerificationMail();
        this.authState = user
      })
      // .then((result) => {
      //   if (result.user.emailVerified !== true) {
      //     this.SendVerificationMail();
      //     window.alert('Please validate your email address. Kindly check your inbox.');
      //   } else {
      //     this.ngZone.run(() => {
      //       this.router.navigate(['userInfo']);
      //     });
      //   }
      //   this.SetUserData(result.user);
      // })
      .catch(error => {
        // console.log(error)
        throw error
      });
  }

  // SendVerificationMail() {
  //   return this.afu.currentUser.
  //   then(u = > u.sendEmailVerification())
  //   .then(() => {
  //     this.router.navigate(['verify-email-address']);
  //   })
  // }
 // Reset Forggot password
 async ForgotPassword(passwordResetEmail) {
  await this.afu.sendPasswordResetEmail(passwordResetEmail)
  .then(() => {
    window.alert('Password reset email sent, check your inbox.');
  }).catch((error) => {
    window.alert(error)
  })
}

  SendVerificationMail() {
    return this.afu.currentUser.then(u => u.sendEmailVerification())
    .then(() => {
    this.router.navigate(['verify-email-address']);
    })
  }


  singout(): void
  {
    this.afu.signOut();
    this.router.navigate(['/login']);
  }

 
  GoogleAuth() {
    return this.AuthLogin(new auth.GoogleAuthProvider());
  }

  FacebookAuth(){
    return this.AuthLogin(new auth.FacebookAuthProvider());
  }

  AuthLogin(provider) {
    return this.afu.signInWithPopup(provider)
    .then((result) => {
       this.ngZone.run(() => {
          this.router.navigate(['/userInfo']);
        })
      this.SetUserData(result.user);
    }).catch((error) => {
      window.alert(error)
    })
  }

  /* Setting up user data when sign in with username/password, 
  sign up with username/password and sign in with social auth  
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
  SetUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified
    }
    return userRef.set(userData, {
      merge: true
    })
  }

  // doGoogleAuth(){
  //   return new Promise((resolve, reject) => {
  //       let provider = new firebase.auth.GoogleAuthProvider();
  //       provider.addScope('profile');
  //       provider.addScope('email');
  //       this.afuAuth.auth
  //       .signInWithPopup(provider)
  //       .then(res => {
  //         resolve(res);
  //       }, err => {
  //         console.log(err);
  //         reject(err);
  //       })
  //     })
  // }
}