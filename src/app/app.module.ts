import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './LoginPage/login/login.component';
import { RegisterComponent } from './LoginPage/register/register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UserInfoComponent } from './LoginPage/user-info/user-info.component';
import { ResetPasswordComponent } from './LoginPage/reset-password/reset-password.component';
import { VerifyEmailAddressComponent } from './LoginPage/verify-email-address/verify-email-address.component';
//Firebase Modules
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from "@angular/fire/auth";
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { FormsModule } from '@angular/forms';
import { AuthserviceService } from './services/authservice.service';
// import { AngularFontAwesomeModule } from 'angular-font-awesome';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    UserInfoComponent,
    ResetPasswordComponent,
    VerifyEmailAddressComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    BrowserAnimationsModule,
    MaterialModule,
    AngularFireAuthModule,
    FormsModule,
    // AngularFontAwesomeModule


  ],
  providers: [AuthserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
