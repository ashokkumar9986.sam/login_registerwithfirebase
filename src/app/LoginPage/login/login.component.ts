import { Component, OnInit } from '@angular/core';
import { AuthserviceService } from '../../services/authservice.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup,FormBuilder ,FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  myForm: FormGroup;
  submitted = false;
  email = "";
  password = "";
  errorMessage = ''; // validation error handle
  error: { name: string, message: string } = { name: '', message: '' }; // for firbase error handle


  
  // myForm = new FormGroup({
  //   email : new FormControl('', [Validators.required, Validators.email]),
  //   password : new FormControl(''),
  // });

  constructor(public _authservice: AuthserviceService,
    private _router: Router,
    private _route: ActivatedRoute,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.myForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
  });
  }

  get f()
   { return this.myForm.controls; }
  
  
  clearErrorMessage() {
    this.errorMessage = '';
    this.error = { name: '', message: '' };
  }

  login()
  {
    this.submitted = true;
      if (this.myForm.invalid) {
          return;
      }
    this.clearErrorMessage();
    if (this.validateForm(this.email, this.password)) {
      this._authservice.loginWithEmail(this.email, this.password)
        .then(() => {
         this._router.navigate(['/userInfo'])
        }).catch(_error => {
          this.error = _error
          this._router.navigate(['/login'])
        })
    }
  }

  validateForm(email, password) {
    if (email.lenght === 0) {
      this.errorMessage = "please enter email id";
      return false;
    }

    if (password.lenght === 0) {
      this.errorMessage = "please enter password";
      return false;
    }

    if (password.lenght < 6) {
      this.errorMessage = "password should be at least 6 char";
      return false;
    }

    this.errorMessage = '';
    return true;

  }
}
