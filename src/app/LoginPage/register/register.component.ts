// import { Component, OnInit } from '@angular/core';
// import { FormGroup } from '@angular/forms';
// import { FormControl } from '@angular/forms';
// import { AuthserviceService } from '../../services/authservice.service';
// import { Router } from '@angular/router';

// @Component({
//   selector: 'app-register',
//   templateUrl: './register.component.html',
//   styleUrls: ['./register.component.css']
// })
// export class RegisterComponent implements OnInit {

  
//   email = "";
//   password="";
//   message = "";
//   errorMessage = "";
//   error: {name:string, message:string} = {name: " ", message: " "} 


//   myForm = new FormGroup({
//     email : new FormControl(''),
//     password : new FormControl(''),
//   });

//   constructor( private _authservice: AuthserviceService,
//     private _router: Router) { }

//   ngOnInit(): void {
//   }

//   clearErrorMessage(){
//     this.errorMessage = '';
//     this.error = { name: '', message: ''}
//   }

//   Register(){
//     this.clearErrorMessage();
//     if(this.validateform(this.email,this.password))
//     {
//       this._authservice.registerwithEmail(this.email,this.password)
//       .then(()=>{
//         this.message = "you are Registered Successfully",
//         this._router.navigate(['/userInfo']);
//       }).catch(_error =>{
//         this.error = _error
//         this._router.navigate(['/register'])
//       })
//     }
//   }

//   validateform(email, password){
//     if(email.length === 0)
//     {
//       this.errorMessage = "Please enter User email ID";
//       return false;
//     }
//     if(password.length === 0){
//       this.errorMessage = "please enter password";
//       return false;
//     }
//     if(password.length < 6){
//       this.errorMessage = "Password should be atleast 6 characters";
//       return false;
//     }

//     this.errorMessage = " ";
//     return true;
//   }

// }


import { Component, OnInit } from '@angular/core';
import { AuthserviceService } from '../../services/authservice.service';
import {Router} from '@angular/router';
import { FormGroup, FormControl} from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email="";
  password="";
  message = '';
  errorMessage = ''; // validation error handle
  error: { name: string, message: string } = { name: '', message: '' }; // for firbase error handle

  myForm = new FormGroup({
        email : new FormControl(''),
        password : new FormControl(''),
      });

  constructor(public authservice: AuthserviceService, private router:Router) { }

  ngOnInit(): void {
  }

  clearErrorMessage()
  {
    this.errorMessage = '';
    this.error = {name : '' , message:''};
  }

  
  Register()
  {
    this.clearErrorMessage();
    if (this.validateForm(this.email, this.password)) {
      this.authservice.registerWithEmail(this.email, this.password)
        .then(() => {
          this.message = "Registration was done successfully"
          this.router.navigate(['/login'])
        }).catch(_error => {
          this.error = _error
          this.router.navigate(['/register'])
        })
    }
  }

  validateForm(email, password)
  {
    if(email.lenght === 0)
    {
      this.errorMessage = "please enter email id";
      return false;
    }

    if (password.lenght === 0) {
      this.errorMessage = "please enter password";
      return false;
    }

    if (password.lenght < 6)
    {
      this.errorMessage = "password should be at least 6 char";
      return false;
    }

    this.errorMessage = '';
    return true;

  }

}